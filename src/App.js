import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import SearchForm from './components/SearchForm';
import ResultPanel from './components/ResultPanel';
import styles from './App.css';

class App extends Component {

    constructor() {
        super();

        this.state = { 
            cities: []
        };
        // this needs to be abstracted
        this.loadInfo();
    }

    loadInfo = () => {
        // lets fake a data source
        fetch('/cities.json')
            .then((response) => {
                return response.json();
            }).then((json) => {
                this.setState({
                    cities: json
                });
            });
    }



    render() {
        return (
            <Router>
            <div className={styles.App}>
                <header className={styles.header}>
                    <h1 className={styles.title}>Searchie</h1>
                    <h4 className={styles.subtitle}>It says search, but it actually doesn't search anything :)</h4>
                </header>
                <SearchForm  cities={this.state.cities} /> 
                <Route exact={true} path="/" render={props => {return (<h3>Please choose two cities to see the distance.</h3>)}} />
                <Route exact={true} path="/:origin" render={props => {return (<h3>You should choose two cities.</h3>)}} />
                <Route exact={true} path="/:origin/:destination" render={props => {return ( <ResultPanel cities={this.state.cities} {...props} />)} } />
            </div>
        </Router>
        );
    }
}

export default App;
