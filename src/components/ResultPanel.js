import React from 'react';


class ResultPanel extends React.Component {

    constructor({match, cities}) {
        super();
        //console.log('match url', match.params) 
        //console.log('cities', cities.length) 

        let distance_text = "We are calculating the distance, please wait";

        if(cities.length > 0) {
            //we already have the cities
            distance_text = this.calculate(cities, match);
        }

        this.state = {
            distance: distance_text
        };
    }


    componentWillReceiveProps(nextProps) {
        if(nextProps.cities.length > 0) {
            let distance_text = this.calculate(nextProps.cities, nextProps.match);
            this.setState({
                distance: distance_text
            })
        }
    }
    
    calculate = (cities, match) => {
        //let's find the cities in the city array
        // this should be done somewhere else, but we are faking the data
       //console.log('calc match url', match.params) 
        const origin = cities.find(city => {
           return (city.rank === match.params.origin)
        })
        //console.log("origin ", origin);
        const destination = cities.find(city => {
           return (city.rank === match.params.destination)
        })
        //console.log("destination ", destination);

        if(origin && destination) {
            // simple calculation of distance, please don't use this code anywhere sensitive
            let lat1 = origin.latitude;
            let lat2 = destination.latitude;
            let lon1 = origin.longitude;
            let lon2 = destination.longitude;
            //function distance(lat1, lon1, lat2, lon2) {
             var p = 0.017453292519943295;    // Math.PI / 180
             var c = Math.cos;
             var a = 0.5 - c((lat2 - lat1) * p)/2 +
                     c(lat1 * p) * c(lat2 * p) *
                     (1 - c((lon2 - lon1) * p))/2;

             let distance = 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
            return `The distance between ${origin.city} and ${destination.city} is ${~~distance} kms.`
        } else {
            return "There seems to be an error. Please choose two cities"
        }
    }

    render() {
        return (
            <div>
                <h3>{this.state.distance}</h3>
            </div>
        )
    }
}

export default ResultPanel;
