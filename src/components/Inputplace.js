import React from 'react';
import { Async } from 'react-select';
import 'react-select/dist/react-select.css';
import styles from './Inputplace.css';

class Inputplace extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: {value: '', label: ''},
            location: props.location,
            cities: []
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            cities: nextProps.cities,
            selectedOption: {
                value: nextProps.cities[0].rank,
                label: nextProps.cities[1].city
            }
        })
    }

    loadOptions = (input, callback) => {
        if(this.state.cities.length > 0) {
            // this should fetch async from a url or store
            // I fake it like this for now
            const cities = this.state.cities.filter((city) => {
                let cityName = city.city.toLowerCase();
                return cityName.startsWith(input);
            }).map(city => {
                return {value: city.rank, label: city.city}
            })
            callback(null, {options: cities, complete:false});
        //} else {
            ////callback(null, {options: null, complete: true});
        }
    }

    handleChange = (selectedOption) => {
        if(selectedOption) {
            this.setState({ selectedOption });
            console.log(`Selected: ${selectedOption.label}`);
            let city = this.state.cities.filter(city => {
                return (city.rank === selectedOption.value);
            })[0];
            this.props.onSelection(this.state.location, city);
        }
    }

    render() {
        return (
            <div className={styles.formcontrol}>
                <p className={styles.title}>{this.props.placeholder}</p>
                <Async
                    name="form-field-name"
                    value={this.state.selectedOption.value}
                    onChange={this.handleChange}
                    isLoading={ false }
                    loadOptions={ this.loadOptions }
                    placeholder = { this.props.placeholder }
                />
            </div>
        )
    }
}

export default Inputplace;
