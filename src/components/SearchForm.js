import React from 'react';
import { Link } from 'react-router-dom';
import Inputplace from './Inputplace';
import styles from './SearchForm.css';

class SearchForm extends React.Component {
    constructor() {
        super();
        this.state = {
            origin: {},
            destination: {}
        };
    }

    onCitySelection = (location, city) => {
        switch(location) {
        case "origin":
            this.setState({
                origin: city
            })
            break; 
        case "destination":
            this.setState({
                destination: city
            })
            break; 
        default:
            break;
        }
    }

    render() {
        const originid = this.state.origin.rank ? this.state.origin.rank : -1;
        const destinationid = this.state.destination.rank ? this.state.destination.rank : -1;
        return (
            <form className={styles.searchform} noValidate>
                <Inputplace cities={this.props.cities} onSelection={this.onCitySelection} location={'origin'} placeholder={'Origin'} />
                <Inputplace cities={this.props.cities} onSelection={this.onCitySelection} location={'destination'} placeholder={'Destination'}/>
                <Link to={`/${originid}/${destinationid}`}>
                    <input className={styles.calcbutton} type="button" onClick={this.props.onCalculate} value='Calculate' />
                </Link>
            </form>
        );
    }

}

export default SearchForm;
